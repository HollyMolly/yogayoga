class Options {
    constructor (height, width, bg, fontSize, textAlign) {
        this.height = height;
        this.width = width;
        this.bg = bg;
        this.fontSize = fontSize;
        this.textAlign = textAlign;
    }
    createDiv () {
        let div = document.createElement('div');
        div.innerHTML = 'Stan Smith is American Dad`s <strong>hero</strong>';
        div.style.cssText = 'height : this.height;' +
            'width: this.width;' +
            'background: this.bg;' +
            'font-size: this.fontSize px;' +
            'text-align: this.textAlign;';
        document.body.appendChild(div);
    }
}

const newObj = new Options(`100px, 200px, red, 10px, center`);
console.log(newObj.createDiv());