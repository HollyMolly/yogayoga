let age = document.getElementById('age'),
    user = {
        old: age.value,
        name: 'Peter',
        surname: 'Griffin',
        showUser: function () {
            alert("Пользователь " + this.surname + " " + this.name + ", его возраст " + this.old);
        }
    };
user.showUser();
